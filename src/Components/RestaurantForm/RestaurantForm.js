import React from "react";
import MultiCheckboxes from "../MultiCheckboxes/MultiCheckboxes";

const RestaurantForm = () => {
  const days = [
    {
      day: "Monday",
    },
    {
      day: "Tuesday",
    },
    {
      day: "Wednesday",
    },
    {
      day: "Thursday",
    },
    {
      day: "Friday",
    },
    {
      day: "Saturday",
    },
    {
      day: "Sunday",
    },
  ];
  return (
    <div>
      <MultiCheckboxes
        days={days}
        onChange={(day) => {
          console.log("To see what day is selected", day);
          // put this day data into react hook form setValue and you are done with this component
        }}
      />
    </div>
  );
};

export default RestaurantForm;
