import { useState } from "react";

const MultiCheckboxes = ({ days, onChange }) => {
  // puts the days in the state fpr re-rendering
  const [data, setData] = useState(days);

  const checkboxTick = (index) => {
    // splice overwrites the original array that's why spreading it to another array
    const newArray = [...data];

    newArray.splice(index, 1, {
      day: data[index].day, // gets the day of the current clicked
      selected: !data[index].selected, // makes the selected inverse of the current clicked (true or false)
    });
    console.log("before", data);
    setData(newArray);

    // when done, put the array in the callback for parent
    let filtered = newArray.filter((item) => item.selected);
    onChange(filtered);
  };

  console.log("after", data);

  return (
    <div
      style={{
        border: "1px solid red",
        padding: 10,
        display: "flex",
        flexDirection: "column",
      }}
    >
      <h5 style={{ textAlign: "center" }}> WEEKDAYS</h5>
      {data?.map((item, index) => {
        return (
          <>
            <label key={item?.day}>
              <input
                type="checkbox"
                onClick={() => checkboxTick(index)}
                checked={item.selected || false}
              />
              {item?.day}
            </label>
          </>
        );
      })}
    </div>
  );
};

export default MultiCheckboxes;
