import RestaurantForm from "./Components/RestaurantForm/RestaurantForm";

function App() {
  return (
    <div style={{ padding: 50 }}>
      <RestaurantForm />
    </div>
  );
}

export default App;
